program z3;
uses crt;
var x1, x2, x3, y1, y2, y3: integer; a, b, c, m: real;
begin
    clrscr;

    write('Введите x1, y1 >');
    readln(x1, y1);
    write('Введите x2, y2 >');
    readln(x2, y2);
    write('Введите x3, y3 >');
    readln(x3, y3);

    a := sqrt(sqr(x1 - x2) + sqr(y2 - y1));
    c := sqrt(sqr(x2 - x3) + sqr(y3 - y2));
    b := sqrt(sqr(x1 - x3) + sqr(y3 - y1));

    m := 0.5 * sqrt( 2*sqr(a) + 2*sqr(b) - sqr(c) );
    writeln(m:8:2);
    readkey;
end.
