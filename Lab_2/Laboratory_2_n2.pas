program z2;
uses crt;
var a, b, c, d, e, f, x, y, delta: real;
begin
    clrscr;
    write('Enter [a, b, c, d, e, f] >');
    readln(a, b, c, d, e, f);

    delta := a*d - b*c;

    x := (e*d - f*b) / delta;
    y := (a*f - c*e) / delta;

    writeln('x = ', x:2:2);
    writeln('y = ', y:2:2);

    readkey;
end.