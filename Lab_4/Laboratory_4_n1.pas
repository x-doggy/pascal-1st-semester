program array_1;
uses crt;
const N=10;
var a: array[1..N] of integer; avg: single; i, sum: integer;
begin
    clrscr;
    
    randomize;
    for i:=1 to N do begin
        a[i] := random(100);
        writeln('A[',i,'] = ', a[i]);
    end;
    
    sum := 0;
    avg := 0.0;
    
    for i:=1 to N do sum := sum + a[i];
    avg := sum / N;
    
    writeln(avg);
    readkey;
end.