program array_function;
uses crt;
const N=1000;
type MyArray = array[1..N] of integer;
var a: MyArray; avg: single; i, sum, elem_amount: integer;

function CountArray(mas: MyArray; M: integer):single;
var j: word; s: single; r: single;
begin
    s := 0;
    for j:=1 to M do s := s + mas[j];
    r := s / M;
    CountArray := r;
end;

begin {main}
    clrscr;

    write('Введите кол-во элементов массива: ');
    readln(elem_amount);

    for i:=1 to elem_amount do begin
        a[i] := random(9090);
        write(a[i], ' ');
    end;

    writeln; writeln;
    writeln('Результат: ', CountArray(a, elem_amount));

    readkey;
end.
