program array_5;
uses crt;
const A=10; B=10;
type TArr = array[1..A, 1..B] of integer;
var C: TArr; i, j: word; fl: boolean;

procedure MyTest(D: TArr; var flag:boolean);
var m, n: byte;
begin
    flag := true;
    for m:=1 to A do
        for n:=1 to B do
                if D[m, n] <> m + n then flag := false;
end;

begin
    clrscr;
    for i:=1 to A do begin
        for j:=1 to B do begin
            C[i, j] := i + j;
            write(C[i][j]:4);
        end;
        writeln;
    end;
    C[2, 3] := 78;
    writeln;
    MyTest(C, fl);
    if fl
        then writeln('Test is complete!')
        else writeln('Not to be complete!');
    readkey;
end.
