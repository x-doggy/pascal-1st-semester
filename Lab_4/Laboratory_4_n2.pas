program array_procedure;
uses crt;
const N=1000;
type MyArray = array[1..N] of integer;
var a: MyArray; avg: single; i, sum, elem_amount: integer;

procedure CountArray(mas: MyArray; M: integer; var r: single);
var j: word; s: single;
begin
    s := 0;
    for j:=1 to M do s := s + mas[j];
    r := s / M;
end;

begin {main}
    clrscr;
  
    write('Введите кол-во элементов массива: ');
    readln(elem_amount);
  
    for i:=1 to elem_amount do begin
      a[i] := random(9090);
      write(a[i], '  ');
    end;
    writeln;
  
    CountArray(a, elem_amount, avg);
  
    writeln;
    writeln('Результат: ', avg);
  
    readkey;
end.
