program z1;
uses crt;
const m=500;
      v=6;
      v1=2;
      qq=10;
type telem=single;
     tarr=array[1..1] of telem;
     parr=^tarr;
     tmas=array[1..1] of parr;
     pmas=^tmas;
     tfile=file of telem;
     porecord = ^myrecord;
     myrecord = record
        num:   integer;
        n1,m1: integer;
        mas:   pmas;
        next:  porecord;
        prev:  porecord;
     end;

{$R-}
procedure FreeMassMem(var x: porecord);
{ Процедура для проверки на хранение в памяти массива, и дальнейшего его удаления}
var need1, need2: longint; i: integer;

begin
    need1 := longint(x^.n1)*sizeof(parr);
    need2 := longint(x^.m1)*sizeof(telem);
    for i:=1 to x^.n1 do freemem(x^.mas^[i], need2);
    freemem(x^.mas, need1);
end;

function CreateOrRemake(var x: PoRecord; Head: PoRecord): byte;
var a, i: integer; need1, need2: longint;
begin
    if head=nil then CreateOrRemake:=1
    else begin
        repeat
            writeln('1) Вы хотите добавить новую таблицу?');
            writeln('2) Вы хотите пересоздать эту таблицу?');
            {$I-} readln(a); {$I+}
        until (ioresult=0) and (a>=1) and (a<=2);
        if a=1 then CreateOrRemake:=1
        else begin
            CreateOrRemake:=0;
            FreeMassMem(x);
            repeat
                writeln('Введите количество столбцов');
                {$I-} readln(x^.n1); {$I+}
            until (ioresult=0) and (x^.n1>0);
            repeat
                writeln('Введите количество строк');
                {$I-} readln(x^.m1); {$I+}
            until (ioresult=0) and (x^.m1>0);
            need1 := longint(x^.n1)*sizeof(parr);
            need2 := longint(x^.m1)*sizeof(telem);
            getmem(x^.mas, need1);
            for i:=1 to x^.n1 do getmem(x^.mas^[i], need2);
        end;
    end;
end;

procedure Create(var x, head, tale: porecord);
{Процедура для задания размеров матрицы}
var need1, need2: longint; i: integer; y: porecord;
begin
    if head=nil then begin
        new(x);
        repeat
            writeln('Введите количество столбцов');
            {$I-} readln(x^.n1); {$I+}
        until (ioresult=0) and (x^.n1>0);
        repeat
            writeln('Введите количество строк');
            {$I-} readln(x^.m1); {$I+}
        until (ioresult=0) and (x^.m1>0);
        need1 := longint(x^.n1)*sizeof(parr);
        need2 := longint(x^.m1)*sizeof(telem);
        getmem(x^.mas, need1);
        for i:=1 to x^.n1 do getmem(x^.mas^[i],need2);
        x^.num := 1;
        x^.next := nil;
        head := x;
        tale := x;
    end else begin
        new(y);
        repeat
            writeln('Введите количество столбцов');
            {$I-} readln(y^.n1); {$I+}
        until (ioresult=0) and (y^.n1>0);
        repeat
            writeln('Введите количество строк');
            {$I-} readln(y^.m1); {$I+}
        until (ioresult=0) and (y^.m1>0);
        need1 := longint(y^.n1)*sizeof(parr);
        need2 := longint(y^.m1)*sizeof(telem);
        getmem(y^.mas, need1);
        for i:=1 to x^.n1 do getmem(y^.mas^[i], need2);
        y^.num := Tale^.num+1;
        Tale^.next := y;
        y^.prev := Tale;
        Tale := y;
        x := Tale;
    end;
end;

Function RandomWriteIn(var x: porecord): byte;
var i, j: integer;
begin
    if x<>nil then begin
        RandomWriteIn := 0;
        for j:=1 to x^.m1 do
            for i:=1 to x^.n1 do
                x^.mas^[i]^[j] := random(201)-100;
    end else RandomWriteIn := 1;
end;

Function CheckInMass(var x: porecord): byte;
var i, j: integer;
begin
    if x<>nil then begin
        CheckInMass := 0;
        for j:=1 to x^.m1 do
            for i:=1 to x^.n1 do
                repeat
                    writeln('Введите элемент номер ', i, ' Строки номер ', j);
                    {$I-} readln(x^.mas^[i]^[j]); {$I+}
                until ioresult=0;
        writeln;
    end else CheckInMass:=1;
end;

Function PrintMass(var x: porecord): byte;
var i, j: integer;
begin
    if x<>nil then begin
        PrintMass := 0;
        for j:=1 to x^.m1 do begin
            for i:=1 to x^.n1 do write(x^.mas^[i]^[j]:v:v1 ,' ');
            writeln;
        end;
    end else PrintMass := 1;
end;

function CheckFileForRead(fil: string; var zu: tfile): byte;
begin
    CheckFileForRead := 0;
    write('Введите имя файла: ');
    readln(fil);
    assign(zu,fil);
    {$I-} reset(zu); {$I+}
    if (ioresult<>0) and (filesize(zu)<>0) then CheckFileForRead := 1;
    close(zu);
end;

procedure ReadFromFile(fil: string; var zu: tfile; var x: porecord; head, tale: porecord);
var n2, m2: single; i, j: integer; y: porecord; need1, need2: longint;
begin
    if head=nil then begin
        new(x);
        assign(zu,fil);
        {$I-} reset(zu); {$I+}
        read(zu,n2);
        read(zu,m2);
        x^.n1 := trunc(n2);
        x^.m1 := trunc(m2);
        need1 := longint(x^.n1)*sizeof(parr);
        need2 := longint(x^.m1)*sizeof(telem);
        getmem(x^.mas, need1);
        for i:=1 to x^.n1 do getmem(x^.mas^[i], need2);
        for i:=1 to x^.n1 do
            for j:=1 to x^.m1 do
                read(zu,x^.mas^[i]^[j]);
        {$I-} close(zu); {$I+}
        x^.num:=1;
        x^.next := nil;
        head := x;
        tale := x;
    end else begin
        new(y);
        assign(zu,fil);
        {$I-} reset(zu); {$I+}
        read(zu,n2);
        read(zu,m2);
        y^.n1 := trunc(n2);
        y^.m1 := trunc(m2);
        need1 := longint(y^.n1)*sizeof(parr);
        need2 := longint(y^.m1)*sizeof(telem);
        getmem(y^.mas, need1);
        for i:=1 to x^.n1 do getmem(y^.mas^[i], need2);
        for i:=1 to y^.n1 do
            for j:=1 to y^.m1 do
                read(zu,y^.mas^[i]^[j]);
        {$I-} close(zu); {$I+}
        y^.num := tale^.num+1;
        tale^.next := y;
        y^.prev := tale;
        tale := y;
        x := tale;
    end;
end;

function SaveInFile(var fil: string; var zu: tfile; var x: porecord): byte;
var i, j: integer;
begin
    if x<>nil then begin
        SaveInFile := 0;
        write('Введите имя файла: ');
        readln(fil);
        assign(zu, fil);
        {$I-} rewrite(zu); {$I+}
        write(zu, x^.n1);
        write(zu, x^.m1);
        for i:=1 to x^.n1 do
            for j:=1 to x^.m1 do
                write(zu, x^.mas^[i]^[j]);
        {$I-} close(zu); {$I+}
    end else SaveInFile := 1;
end;

procedure ChooseYourTable(var x, head, tale: porecord);
var n: integer;
begin
    repeat
        write('Выберите таблицу, Введите № от 1 до ', Tale^.num, ': ');
        {$I-} readln(n); {$I+}
    until (ioresult=0) and (n>=1) and (n<=Tale^.num);
    x := head;
    while (x^.num<>n) do x := x^.next;
end;



var y, coli, Tab, m1, n1: integer;
    fil: string;
    mas: pmas;
    zu: tfile;
    head: porecord = nil;
    x: porecord = nil;
    tale: porecord = nil;

begin {main}
    repeat
        clrscr;
        if x=nil then Tab:=0 else tab:=x^.num;
        writeln('Таблица № ',Tab);
        writeln('1. Задать размеры');
        writeln('2. Заполнить Случайными числами');
        writeln('3. Заполнить с клавиатуры');
        writeln('4. Просмотр таблицы');
        //writeln('5. Выполнить');
        writeln('6. Вывод информации из файла');
        writeln('7. Сохранить в файл ');
        writeln('8. Выбрать таблицу ');
        writeln('9. Выход');
        write('Введите номер пункта меню: ');
        readln(y);

        case y of
            1:
            begin
                case CreateOrRemake(x, head) of
                    1:Create(x, head, tale);
                    0:writeln('Успешно');
                end;
            end;
            2:
            begin
                case RandomWriteIn(x) of
                    1:Writeln('Массив не создан');
                    0:Writeln('Случайное заполнение успешно');
                end;
            end;
            3:
            begin
                case CheckInMass(x) of
                    1:Writeln('Массив не создан');
                    0:Writeln('Заполнение успешно завершенно');
                end;
            end;
            4:
            begin
                case PrintMass(x) of
                    1:Writeln('Массив не создан');
                    0:Writeln('Вывод на экран успешно выполнен');
                end;
            end;
            6:
            begin
                case CheckFileForRead(fil, zu) of
                    1:Writeln('Файл не найден');
                    0:ReadFromFile(fil, zu, x, head, tale);
                end;
            end;
            7:
            begin
                case SaveInFile(fil, zu, x) of
                    1:Writeln('Нечего сохранять');
                    0:Writeln('Сохранение успешно');
                end;
            end;
            8: ChooseYourTable(x, head, tale);
        end;
        if y<>9 then readkey;
    until (y=9);

    {FreeMassMem(x);
    dispose(x); }
    {$R+}

end.
