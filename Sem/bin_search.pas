{$R-}
program binary_search;

const MAX = 32767;
type TReal = single;
     TIndex = 0..MAX;
     TArrayReal = array[1..1] of TReal;
     PArrayRealDyn = ^TArrayReal;

(*
  Процедура бинарного поиска элемента x в массиве a размера n.
  Выходные переменные: res - индекс найденного элемента,
  CE - false, если входные параметры некорректны, либо элемент не найден. 
*)
Procedure BinSearch(var a: PArrayRealDyn; n: TIndex; x: TReal; var res: TIndex; var CE: boolean);
var i, mid, left, right: TIndex; ok: boolean;
begin
    if (a = NIL) or (n<1) then CE := false
    else begin
        left := 1; right := n;
        res := 0; ok := true;

        while (right >= left) and ok do begin
            mid := (left + right) div 2;
            if a^[mid] = x then begin
                res := mid;
                right := left - 1;
                ok := false;
            end;
            if a^[mid] > x then right := mid - 1;
            if a^[mid] < x then left  := mid + 1;
        end;
    end;
    
    CE := (res < 1);
end; { / procedure }

var a: PArrayRealDyn; i, found, n: TIndex; ce: boolean; x: TReal; need: longint;

begin

  a := NIL;
  write('Enter array''s size: ');
  readln(n);
  
  need := longint(n) * sizeof(TReal);
  
  getmem(pointer(a), longint(n) * sizeof());
  
  for i := 1 to n do begin
    write('A[', i, '] = ');
    readln(a^[i]);
  end;
  
  write('Element to search: ');
  readln(x);
  
  BinSearch(a, n, x, found, ce);
  
  if ce then writeln('FOUND: ', found) else writeln('NOT FOUND');
  
  freemem(a, need);

end.
