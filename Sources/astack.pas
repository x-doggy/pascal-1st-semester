Unit AStack;
Interface
  {стек символов}
  Type
    TElemCh  = Char;
  Type
    PStackCh = ^CStackCh;
    CStackCh = Object
    public
      {основные операции}
      constructor Create; {инициировать стек}
      destructor Destroy; virtual; {разрушить стек}
      function IsEmpty: Boolean; {стек пуст?}
      procedure Push({var} x:TElemCh); {добавить в стек}
      procedure Pop(var x:TElemCh); {взять из стека}
      function  SeeTop:TElemCh;
      {дополнительные и служебные операции}
    private
      {фиксация/обработка ошибок}
      procedure Failure(n:Byte); {завершить аварийно}
      {вспомогательные функции}
    private
      {основные поля для одного из вариантов реализации}
      Ptr:Pointer;
      {дополнительные и служебные поля}
    end;
  {стек вещественных чисел}
  Type
    _Real   = Single;
    TElemRe = _Real;
  Type
    PStackRe = ^CStackRe;
    CStackRe = Object
                public
                 {основные операции}
                 constructor Create; {инициировать стек}
                 destructor Destroy; virtual; {разрушить стек}
                 function IsEmpty: Boolean; {стек пуст?}
                 procedure Push({var} x:TElemRe); {добавить в стек}
                 procedure Pop(var x:TElemRe); {взять из стека}
                 function  SeeTop:TElemRe;
                 {дополнительные и служебные операции}
               private
                 {фиксация/обработка ошибок}
                 procedure Failure(n:Byte); {завершить аварийно}
                 {вспомогательные функции}
               private
                 {основные поля для одного из вариантов реализации}
                 Ptr:Pointer;
                 {дополнительные и служебные поля}
    end;
   Type
    TElemBo  = boolean;
   Type
     PStackBo = ^CStackBo;
     CStackBo = Object
     public
       {основные операции}
       constructor Create; {инициировать стек}
       destructor Destroy; virtual; {разрушить стек}
       function IsEmpty: Boolean; {стек пуст?}
       procedure Push({var} x:TElemBo); {добавить в стек}
       procedure Pop(var x:TElemBo); {взять из стека}
       function  SeeTop:TElemBo;
       {дополнительные и служебные операции}
     private
       {фиксация/обработка ошибок}
       procedure Failure(n:Byte); {завершить аварийно}
       {вспомогательные функции}
     private
       {основные поля для одного из вариантов реализации}
       Ptr:Pointer;
       {дополнительные и служебные поля}
     end;
  {другие стеки}
Implementation
  Uses Crt;
  {реализация стека символов на основе односвязного списка}
  Type
    PLinkCh = ^TLinkCh;
    TLinkCh = Record
      Elem:TElemCh;
      Next:PLinkCh;
    end;
  constructor CStackCh.Create;
  begin
    Ptr:=nil;
  end;
  destructor CStackCh.Destroy;
  var V:PLinkCh;
  begin
    while Ptr <> nil do
    begin
      V:=Ptr;
      Ptr:=V^.Next;
      dispose(V);
    end;
  end;
  function CStackCh.IsEmpty;
  begin
    IsEmpty:=(Ptr=nil);
  end;
  procedure CStackCh.Push;
  var V:PLinkCh;
  begin
    if MaxAvail < Sizeof(TLinkCh) then
      Failure(1)
    else
    begin
      new(V); V^.Elem:=x;
      V^.Next:=Ptr; Ptr:=V;
    end;
  end;
  procedure CStackCh.Pop;
  var V : PLinkCh;
  begin
    if Ptr = nil then
      Failure(2)
    else
    begin
      V:=Ptr; x:=V^.Elem;
      Ptr:=V^.Next; dispose(V);
    end;
  end;
  function  CStackCh.SeeTop;
  begin
    if Ptr = nil then
      Failure(3)
    else
      SeeTop:=PLinkCh(Ptr)^.Elem;
  end;

  {фиксация/обработка ошибок стека символов}
  procedure CStackCh.Failure;
  begin
    writeln; writeln('Ошибка CStackCh # ',n:1);
    case n of
      1: writeln('Метод Push: недостаточно памяти');
      2: writeln('Метод Pop: стек пуст');
    end;
    Halt(1); {выход в операционную среду}
  end;

  {реализация стека чисел на основе односвязного списка}
  Type
    PLinkRe = ^TLinkRe;
    TLinkRe = Record
      Elem:TElemRe;
      Next:PLinkRe;
    end;
  constructor CStackRe.Create;
  begin
    Ptr:=nil;
  end;

  destructor CStackRe.Destroy;
  var V:PLinkRe;
  begin
    while Ptr <> nil do
    begin
      V:=Ptr;
      Ptr:=V^.Next;
      dispose(V);
    end;
  end;
  function CStackRe.IsEmpty;
  begin
    IsEmpty:=(Ptr=nil);
  end;
  procedure CStackRe.Push;
  var V:PLinkRe;
  begin
    if MaxAvail < Sizeof(TLinkRe) then
      Failure(1)
    else
    begin
      new(V);
      V^.Elem:=x;
      V^.Next:=Ptr;
      Ptr:=V;
    end;
  end;
  procedure CStackRe.Pop;
  var V : PLinkRe;
  begin
    if Ptr = nil then
      Failure(2)
    else
    begin
      V:=Ptr;
      x:=V^.Elem;
      Ptr:=V^.Next;
      dispose(V);
    end;
  end;
  function  CStackRe.SeeTop;
  begin
    if Ptr = nil then
      Failure(3)
    else
      SeeTop:=PLinkRe(Ptr)^.Elem;
  end;

  {фиксация/обработка ошибок}
  procedure CStackRe.Failure;
  begin
    writeln; writeln('Ошибка CStackRe # ',n:1);
    case n of
      1: writeln('Метод Push: недостаточно памяти');
      2: writeln('Метод Pop: стек пуст');
    end;
    Halt(1); {выход в операционную среду}
  end;

  {реализация стека логических величин на основе односвязного списка}
  Type
    PLinkBo = ^TLinkBo;
    TLinkBo = Record
      Elem:TElemBo;
      Next:PLinkBo;
    end;
  constructor CStackBo.Create;
  begin
    Ptr:=nil;
  end;
  destructor CStackBo.Destroy;
  var V:PLinkBo;
  begin
    while Ptr <> nil do
    begin
      V:=Ptr;
      Ptr:=V^.Next;
      dispose(V);
    end;
  end;
  function CStackBo.IsEmpty;
  begin
    IsEmpty:=(Ptr=nil);
  end;
  procedure CStackBo.Push;
  var V:PLinkBo;
  begin
    if MaxAvail < Sizeof(TLinkBo) then
      Failure(1)
    else
    begin
      new(V); V^.Elem:=x;
      V^.Next:=Ptr; Ptr:=V;
    end;
  end;
  procedure CStackBo.Pop;
  var V : PLinkBo;
  begin
    if Ptr = nil then
      Failure(2)
    else
    begin
      V:=Ptr; x:=V^.Elem;
      Ptr:=V^.Next; dispose(V);
    end;
  end;
  function  CStackBo.SeeTop;
  begin
    if Ptr = nil then
      Failure(3)
    else
      SeeTop:=PLinkBo(Ptr)^.Elem;
  end;

  {фиксация/обработка ошибок стека символов}
  procedure CStackBo.Failure;
  begin
    writeln; writeln('Ошибка CStackBo # ',n:1);
    case n of
      1: writeln('Метод Push: недостаточно памяти');
      2: writeln('Метод Pop: стек пуст');
    end;
    Halt(1); {выход в операционную среду}
  end;

end.

