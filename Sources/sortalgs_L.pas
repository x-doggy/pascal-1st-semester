uses crt;
const n_max = 100;
type
TUnSignedInt = word;
TSignedInt = integer;
TLongSignedInt = longint;
TKey
= TUnSignedInt;
TStep
= TUnSignedInt;
TElem
= record
key : TKey;
data : ...; {Некий произвольный тип данных}
end;
TArray
= array[0..n_max] of TElem;
TIntArr
= array[1..10] of TStep;procedure InsertionSort1(var a : TArray; n : TUnSignedInt);
var
i,j : TUnSignedInt;
x : TElem;
begin
for i:=2 to n do
begin
x:=a[i];
{Запоминаем текущий элемент}
a[0]:=x;
{Кладем текущий элемент в фиктивный элемент массива}
j:=i-1;
while x.key<a[j].key do {За счет буферного элемента можно не проверять}
begin
{условие выхода за пределы массива,}
a[j+1]:=a[j];
{поскольку элемент x все равно встретится.}
dec(j);
end;
a[j+1]:=x;
end;
end;procedure InsertionSort2 (var a : TArray; n : TUnSignedInt);
var
i,j : word;
x : TElem;
begin
for i:=2 to n do
begin
x:=a[i];
j:=i-1;
while (j>0)and(x.key<a[j].key) do {Проверка выхода за пределы необходима}
begin
a[j+1]:=a[j];
dec(j);
end;
a[j+1]:=x;
end;
end;procedure SelectionSort(var a : TArray; n : TUnSignedInt);
var
i,j,k : word;
x : TElem;
begin
for i:=1 to n-1 do
begin
k:=i;
x:=a[i];
for j:=i+1 to n do
if a[j].key<x.key then
begin
k:=j;
x:=a[j];
end;
a[k]:=a[i];
a[i]:=x;
end;
end;
{Выбираем первый элемент с наименьшим ключом}procedure BubbleSort(var a : TArray; n : TUnSignedInt);
var i,j : word;
x : TElem;
begin
for i:=2 to n do
begin
for j:=n downto i do
if a[j-1].key>a[j].key then
begin
x:=a[j-1];
a[j-1]:=a[j];
a[j]:=x;
end;
end;
end;procedure BoundedBubbles(var a : TArray; n : TUnSignedInt);
var i,j,k,l : word;
x
: TElem;
begin
k:=2;
for i:=2 to n do
begin
l:=n+1;
for j:=n downto k do
{Если k=n+1, то цикл не выполнится ни разу}
if a[j-1].key>a[j].key then
begin
x:=a[j-1];
a[j-1]:=a[j];
a[j]:=x;
l:=j;
end;
k:=l;
end;
end;
procedure ShellSort(var a:TArray; n:TUnSignedInt; var h:TIntArr; t:TUnSignedInt);
var i,j : TLongSignedInt; {Чтобы не возникало переполнения в типе TUnSignedInt}x : TElem;
{в строке (*)}
m : TUnSignedInt;
k : TStep;
begin
for m:=1 to t do
begin
k:=h[m];
{Запоминаем текущий шаг}
for i:=k+1 to n do
begin
x:=a[i];
j:=i-k;
while (j>0)and(x.key<a[j].key) do
{Просмотр готовой последовательности}
begin
a[j+k]:=a[j];
{(*)} j:=j-k;
end;
a[j+k]:=x;
end;
end;
end;procedure QuickSort(var a : TArray; n : TUnSignedInt);
procedure Sort(l,r: TUnSignedInt);
var
i,j : TUnSignedInt;
x,w : TElem;
begin
i:=l;
j:=r;
x:=a[(l+r) div 2];
repeat
while a[i].key<x.key do
inc(i);
while x.key<a[j].key do
dec(j);
if i<=j then
begin
w:=a[i];
a[i]:=a[j];
a[j]:=w;
inc(i);
dec(j);
end;
until i>j;
if l<j then Sort(l,j);if i<r then Sort(i,r)
end;
begin
Sort(1,n);
end;procedure SimpleMergeSort(var a : TArray; n : TUnSignedInt);
type
PLine = ^TLine;
TLine = array[1..1] of TElem;
var b
: PLine;
i,j,k,l,t,h,m,p,q,r : TUnSignedInt;
up
: boolean;
begin
getmem(b,2*n*sizeof(TElem));
{Вспомогательный массив двойной длины}
for i:=1 to n do
b^[i]:=a[i];
up:=true;
{Направление перемещения элементов}
p:=1;
repeat
h:=1;
m:=n;
if up then
begin
i:=1;
{Начало последовательности a (из которой читаем)}
j:=n;
{Начало последовательности b (из которой читаем)}
k:=n+1;
{Начало последовательности a (в которую пишем)}
l:=2*n;
{Начало последовательности b (в которую пишем)}
end
elsebegin
k:=1;
{Начало последовательности a (в которую пишем)}
l:=n;
{Начало последовательности b (в которую пишем)}
i:=n+1;
{Начало последовательности a (из которой читаем)}
j:=2*n;
{Начало последовательности b (из которой читаем)}
end;
repeat
if m>=p then
{q - длина серии из последовательности a}
q:=p
{Если остаток еще не рассмотренного массива}
else
{меньше степени 2 (очередной длины подпоследовательности),}
q:=m;
{то берется столько, сколько осталось}
m:=m-q;
if m>=p then
{r - длина серии из последовательности b (аналогично)}
r:=p
else
r:=m;
m:=m-r;
while (q>0)and(r>0) do
{Пока не кончилась одна из серий.}
begin
if b^[i].key<b^[j].key then
begin
b^[k]:=b^[i];
k:=k+h;
i:=i+1;
q:=q-1;
endelse
begin
b^[k]:=b^[j];
k:=k+h;
j:=j-1;
r:=r-1;
end;
end;
while r>0 do
begin
b^[k]:=b^[j];
k:=k+h;
j:=j-1;
r:=r-1;
end;
while q>0 do
begin
b^[k]:=b^[i];
k:=k+h;
i:=i+1;
q:=q-1;
end;
h:=-h;
{Когда сольются две очередные серии, меняется направление}
t:=k;
{записи, и запись будет идти с другого конца текущей части}
{массива.}
k:=l;l:=t;
until m=0;
up:=not up; {После каждого прохода сортировки половинки массива}
{меняются ролями.}
p:=p*2;
{Длина серии увеличивается в 2 раза после каждого прохода.}
until p>=n;
if up then
{Копируем из вспомогательного обратно в исходный.}
for i:=1 to n do
a[i]:=b^[i]
else
for i:=1 to n do
a[i]:=b^[i+n];
freemem(b,2*n*sizeof(TElem));
end;type
TKey = byte;
const
TKeyMax = 255;
{Т.к. тип ключей - byte}
type
TColArr=array[1..TKeyMax] of TUnSignedInt;
procedure CountingSort(var a : TArray; n,k : TKey);
var
b : TArray;
c : TColArr;
i,j : TKey;
begin
for i:=1 to k do
c[i]:=0;
for j:=1 to n do
c[a[j].key]:=c[a[j].key]+1;
for i:=2 to k do
c[i]:=c[i]+c[i-1];
for j:=n downto 1 do
begin
b[c[a[j].key]]:=a[j];
c[a[j].key]:=c[a[j].key]-1;
end;
for j:=1 to n doa[j]:=b[j];
end ;
