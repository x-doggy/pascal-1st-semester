unit AL1List;
interface
type
TKey = byte;
TElem = record
key : TKey;
data : integer;
end;
PL1List = ^Clist1;
CList1 = object
private
pb,pt : pointer;
ErrCode : byte;
public
constructor Init;
constructor Copy(pl1:PL1List);
destructor Done;
procedure MakeEmpty;
function IsEmpty:boolean;
procedure ToBegin;
function IsEnd:boolean;
procedure ToNext;procedure Insert(x:TElem);
procedure Extract(var x:TElem);
procedure See(var x : TElem);
procedure Put(x:TElem);
procedure Delete;
function GetError:byte;
private
procedure SetError(mask:byte);
procedure ClearError(mask:byte);
end;
implementation
type PBlock = ^TBlock;
TBlock = record
data : TElem;
next : PBlock;
end;program NatMerge;
uses AL1List, crt;
procedure listing(p:PL1List);
var
e : TElem;
begin
p^.ToBegin;
while not p^.IsEnd do
begin
p^.See(e);
write(e.key,' ');
p^.ToNext;
end;
readln;
end;
procedure NaturalMerge(var pc:PL1List);
var
pa,pb : PL1List;RunCol : word;
EndOfRun : boolean;
procedure Copy(var p1,p2 :PL1List; var EndOfRun:boolean);
var
e1,e2 : TElem;
begin
p1^.Extract(e1); {Изымаем всегда только начало, а пишем только в конец.}
p2^.Insert(e1); {При добавлении указатель становится не в конце списка}
p2^.ToNext;
if p1^.IsEmpty then EndOfRun:=true
else
begin
p1^.See(e2);
EndOfRun:=(e1.key>e2.key);
end;
end;
procedure CopyRun(var p1,p2:PL1List; EndOfRun:boolean);
beginrepeat
copy(p1,p2, EndOfRun);
until EndOfRun;
end;
procedure Distribute(var pc,pa,pb: PL1List; EndOfRun:boolean);
begin
repeat
CopyRun(pc,pa,EndOfRun);
if not pc^.IsEmpty then
CopyRun(pc,pb,EndOfRun);
until pc^.IsEmpty;
end;
procedure MergeRun(var pc,pa,pb: PL1List; var EndOfRun:boolean);
var
e1,e2 : TElem;
begin
repeatpa^.See(e1);
pb^.See(e2);
if e1.key<=e2.key then
begin
Copy(pa,pc,EndOfRun);
if EndOfRun then CopyRun(pb,pc,EndOfRun);
end
else
begin
Copy(pb,pc,EndOfRun);
if EndOfRun then CopyRun(pa,pc,EndofRun);
end;
until EndOfRun;
end;
procedure Merge(var pc,pa,pb: PL1List; EndOfRun:boolean; var RunCol: word);
begin
RunCol:=0;
while (not pa^.IsEmpty)and(not pb^.IsEmpty) do
begin
MergeRun(pc,pa,pb,EndOfRun);
RunCol:=RunCol+1;end;
while not pa^.IsEmpty do
begin
CopyRun(pa,pc,EndOfRun);
RunCol:=RunCol+1;
end;
while not pb^.IsEmpty do
begin
CopyRun(pb,pc,EndOfRun);
RunCol:=RunCol+1;
end;
end;
begin
new(pa,Init);
new(pb,Init);
repeat
pc^.ToBegin;
Distribute(pc,pa,pb,EndOfRun);
write('a: ');
listing(pa);
writeln;write('b: ');
listing(pb);
writeln;
RunCol:=0;
pa^.ToBegin;
pb^.ToBegin;
Merge(pc,pa,pb,EndOfRun,RunCol);
write('c: ');
listing(pc);
writeln;
until RunCol=1;
dispose(pa,Done);
dispose(pb,Done);
end;
var pl1 : PL1List;
e : TElem;
i : integer;
begin
randomize;new(pl1,Init);
for i:=1 to 20 do
begin
e.key:=random(10);
e.data:=i;
pl1^.Insert(e);
pl1^.ToNext;
end;
listing(pl1);
pl1^.ToBegin;
NaturalMerge(pl1);
listing(pl1);
dispose(pl1,Done);
end.
