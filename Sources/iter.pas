unit List1Types;
interface
type TElem = ...;
PEnumList1=^CEnumList1;
CEnumList1=object
private
Pt, Pe : pointer;
public
constructor Init(PStart, PEnd : pointer);
destructor Done; virtual;
procedure ReadData(var x : TElem);
function
IsNotEnd : boolean;
end;
PStack = ^CStack;
... { стек }
procedure StartEnum(var PEnum : PEnumList1);
...
PQueue = ^CQueue;
... { очередь }
procedure StartEnum(var PEnum : PEnumList1);
...
PSequence = ^CSequence;
... { последовательность }
procedure StartEnum(var PEnum : PEnumList1);
...
PSequence = ^CList1;
... { Л1– список }
procedure StartEnum(var PEnum : PEnumList1);
...implementation
type PBlock = ^TBlock;
TBlock = record
data : TElem;
next : PBlock;
end;
{ Реализация стека. }
...
procedure CStack.StartEnum(var PEnum : PEnumList1);
begin
new(PEnum,Init(PTop,nil));
end;
...
{ Реализация очереди. }
...
procedure CQueue.StartEnum(var PEnum : PEnumList1);
begin
new(PEnum,Init(PHead,PTail)); { если с буферным элементом}
{new(PEnum, Init(PHead,nil));} { если без буферного элемента}
end;
...
{ Реализация последовательности. }
...
{ Так же, как у очереди }
...
{ Реализация Л1– списка }
...procedure CSequence.StartEnum(var PEnum : PEnumList1);
begin
new(PEnum,Init(PBlock(PBeg)^.next, PBeg)); { если закольцованный с буферным элементом}
{new(PEnum, Init(PBeg,nil));} { если незакольцованный без буферного элемента}
end;
...
{ Реализация итератора }
constructor CEnumList1.Init;
begin
Pt:=PStart;
Pe:=PEnd;
end;
destructor CEnumList1.Done;
begin
end;
procedure CEnumList1.ReadData;
begin
{ Проверка существования элемента и обработка ошибки }
x:=PBlock(Pt)^.data;
Pt:=PBlock(Pt)^.next;
end;function CEnumList1.IsNotEnd;
begin
IsNotEnd:=(Pt<>Pe); { Это можно делать только если точно известно, что эти указатели ранее
получались }
end;
{ друг из друга присваиванием. В случае наших АТД – это так.}
...
end.
Пример использования итератора для очереди:
uses AList1Types;
...
var
pq : PQueue;
x
: TElem;
PIter : PEnumList1;
...
begin
...
pq^.StartEnum(PIter);
while PIter^.IsNotEnd do
begin
PIter^.ReadData(x);
{ Обработка значения x}
end;
...
dispose(PIter, Done);
...
end.
