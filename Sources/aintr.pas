Unit AIntr;
 interface
  uses AStream, AStack, Apro;

  type
       TLexema=(opar {(},cpar {)},sgn {знак операции},value {операнд});
       PIntr  =^CIntr;
       CIntr  = object
                 public
                  constructor Create(Pst:PFStreamTxt);
                  destructor  Destroy; virtual;
                  procedure   Interpretate;
                 private
                  p   : PFStreamTxt;
                  Pro : PPro;
                  St  : PStack;        {Стек Дейкстры}
                  OldLex : TLexema;
                  procedure Failure(n:byte);
                  procedure Lexem;     {Обработка очередной лексемы}
                  procedure Number;    {Обработка числового операнда}
                  function  Weight(x:char):byte; {Выдает вес операции или скобки}
                  function  Right(x:byte):boolean;{true - для правоассоциативных}
                  procedure PushPop(x:char); {Обработка отложенных операций вместе
                                       с помещением в стек рассматриваемой лексемы}
                end;

 implementation
  uses crt;

  constructor CIntr.Create;

  begin
   p:=pst;
   writeln('Начать работу');
   new(Pro,Create);
   new(St,Create);
  end;

  destructor CIntr.Destroy;

  begin
   dispose(St,Destroy);    {Поток не разрушается}
   writeln('Закончить работу');
   dispose(Pro,Destroy);
   p:=nil;
  end;

  procedure CIntr.Interpretate;
   var s : string;

  begin
   St^.Push('(');
   OldLex:=opar;
   while not p^.IsEnd do
    Lexem;
   if not ((OldLex=value)or(OldLex=cpar)) then
    Failure(1);
   PushPop(')');
   if not St^.IsEmpty then
    Failure(2);
   writeln('Показать результат');
   Pro^.Top2Str(s);
   writeln('Ответ= ',s);
   Pro^.Del;
  end;

  procedure CIntr.Lexem;
   var c:char;

  begin
   c:=p^.GetChar;
   case c of
    '0'..'9': begin
               if not ((OldLex=opar)or(OldLex=sgn)) then Failure(11);
               Number;
               OldLex:=value;
              end;
    '('     : begin
               if not ((OldLex=opar)or(Oldlex=sgn)) then Failure(12);
               St^.Push(c);
               p^.Skip;
               OldLex:=opar;
              end;
    ')'     : begin
               if not ((OldLex=cpar)or(Oldlex=value)) then Failure(13);
               PushPop(c);
               p^.Skip;
               OldLex:=cpar;
              end;
    '+','-','*','/'
            : begin
               if (c='+')and((OldLex=opar)or(Oldlex=sgn)) then
                begin
                 if not((Weight('p')>Weight(St^.SeeTop))or
                 ((Weight('p')=Weight(St^.SeeTop))and
                 Right(Weight('p')))) then Failure(14);
                 p^.Skip;
                 OldLex:=sgn;
                end
               else
                if (c='-')and((OldLex=opar)or(OldLex=sgn)) then
                 begin
                  if not((Weight('m')>Weight(St^.SeeTop))or
                  ((Weight('m')=Weight(St^.SeeTop))and
                  Right(Weight('m')))) then Failure(15);
                  PushPop('m');
                  p^.Skip;
                  OldLex:=sgn;
                 end
                else
                 if c in ['+','-','*','/'] then
                  begin
                   if not((OldLex=value)or(OldLex=cpar)) then
                    Failure(16);
                   PushPop(c);
                   p^.Skip;
                   OldLex:=sgn;
                  end;
              end;
    else
     Failure(17);
   end;
  end;

  procedure CIntr.PushPop;
   var y:char;

  begin
   while true do
    begin
     if St^.IsEmpty then Failure(21);
     if ((Weight(x)>Weight(St^.SeeTop))or
     ((Weight(x)=Weight(St^.SeeTop))and Right(Weight(x)))) then break
     else
      begin
       St^.Pop(y);
       case y of
        'm':begin
             writeln('Изменить знак');
             Pro^._Inv;
            end;
        '+':begin
             writeln('Сложить');
             Pro^._Add;
            end;
        '-':begin
             writeln('Вычесть');
             Pro^._Sub;
            end;
        '*':begin
             writeln('Умножить');
             Pro^._Mult;
            end;
        '/':begin
             writeln('Разделить');
             Pro^._Div;
            end;
       end;
      end;
    end;
   if x in ['m','+','-','*','/'] then St^.Push(x)
   else
    if x=')' then
     begin
      if not(St^.SeeTop='(') then Failure(22);
      St^.Pop(y);
     end;
  end;

  function CIntr.Weight;
  begin
   case x of
    '(':Weight:=0;
    ')':Weight:=1;
    '+','-':Weight:=2;
    '/':Weight:=3;
    '*':Weight:=4;
    'p','m':Weight:=5;
   else
    Failure(31);
   end;
  end;

  function CIntr.Right;
  begin
   case x of
    2:Right:=false;
    3:Right:=true;
    4:Right:=false;
    5:Right:=true;
    else
     Right:=false;
   end;
  end;

  procedure CIntr.Number;

   function dig(c:char):byte;

   begin
    if c in ['0'..'9'] then
     dig:=ord(c)-ord('0')
    else
     dig:=0;
   end;

 begin
  ...
 end;

 procedure CIntr.Failure;

 begin
  {Перечисление ошибок.}
  halt(1);
 end;

end.
