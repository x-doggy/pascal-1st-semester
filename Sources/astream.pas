Unit AStream;
Interface
  {входной поток значащих символов на основе текстового файла}
  Type
    PFStreamTxt = ^CFStreamTxt;
    CFStreamTxt = Object
    public
      constructor Create(DataName:String); {создать и открыть поток}
      destructor Destroy; virtual; {разрушить поток}
      {основные операции}
      function GetChar:Char; {текущий значащий символ}
      function IsAfterSpace:Boolean; {после символа форматирования?}
      procedure Skip;{перейти к следующему значащему символу}
      function IsEnd:Boolean; {конец потока?}
      {дополнительные и служебные функции}
    private
      {фиксация/обработка ошибок}
      procedure Failure(n:Byte); {завершить аварийно}
      {вспомогательные функции}
      procedure SkipSpaces; {перейти к следующему значащему символу
                             или встать в конец потока}
    private
      {основные поля для одного из вариантов реализации}
      DN:String; {имя набора данных}
      F:Text;
      CharBufer:Char;
      AfterSpaceBufer:Boolean;
      EndOfStream:Boolean;
      {дополнительные и служебные поля}
    end;
  {другие входные потоки значащих символов}

Implementation
  Uses Crt;
  {!символы форматирования в кодировке ASCII!}
  Const SetOfSpaces:Set of Char=[#9,' ',#255];
  {основные операции потока из текстового файла}
  constructor CFStreamTxt.Create;
  begin
    DN:=DataName; EndOfStream:=true;
    {$I-} {отключить обработку ошибок ввода-вывода}
    Assign(F,DN); Reset(F);
    {$I+} {включить обработку ошибок ввода-вывода}
    if IOResult <> 0 then
      Failure(1)
    else
    begin
      EndOfStream:=false;
      SkipSpaces; {пропустить символы форматирования из начала файла}
    end;
  end;
  destructor CFStreamTxt.Destroy;
  begin
    Close(F); EndOfStream:=true; DN:='';
  end;
  function CFStreamTxt.GetChar;
  begin
    if EndOfStream then
      Failure(2)
    else
     GetChar:=CharBufer;
  end;
  function CFStreamTxt.IsAfterSpace;
  begin
    if EndOfStream then
      Failure(3)
    else
      IsAfterSpace:=AfterSpaceBufer;
  end;
  procedure CFStreamTxt.Skip;
  begin
    if EndOfStream then
      Failure(4)
    else
      SkipSpaces;
  end;
  function CFStreamTxt.IsEnd;
  begin
    IsEnd:=EndOfStream;
  end;
  {дополнительные и служебные операции потока из текстового файла}
  {фиксация/обработка ошибок потока из текстового файла}
  procedure CFStreamTxt.Failure;
  begin
    writeln; writeln('Ошибка CFStreamTxt # ',n:1);
    case n of
      1: writeln('Метод Create: набор данных ',DN,' не найден');
      2: writeln('Метод GetChar: конец потока');
      3: writeln('Метод IsAfterSpace: конец потока');
      4: writeln('Метод Skip: конец потока');
    end;
    Halt(1); {выход в операционную среду}
  end;
  {вспомогательные функции потока из текстового файла}
  procedure CFStreamTxt.SkipSpaces;
  begin
    AfterSpaceBufer:=false;
    while true do
    begin
      if Eof(F) then
      begin
        EndOfStream:=true; break;
      end;
      if Eoln(F) then
        readln(F)
      else
      begin
        read(F,CharBufer);
        if not(CharBufer in SetOfSpaces) then break;
      end;
      AfterSpaceBufer:=true;
    end;
  end;

end.

