Unit APro;
Interface
  {стековый процессор}
  Uses AStack;
  Type
    PPro = ^CPro;
    CPro = Object
    public
      constructor Create; {инициировать процессор}
      destructor Destroy; virtual; {разрушить процессор}
      {загрузка/считывание}
      procedure LoadB(b:Boolean); {загрузить логическое}
      procedure LoadR(x:_Real); {загрузить вещественное}
      procedure Del; {удалить вершину "стека" процессора}
      procedure Top2Str(var S:String); {строковое значение
                                        вершины "стека"}
      {function GetTopType:Char; {тип значения вершины}
      {арифметические операции}
      {procedure _Power; {степень}
      procedure _Mult; {умножение}
      procedure _Div; {деление}
      procedure _Add; {сложение}
      procedure _Sub; {вычитание}
      procedure _Inv; {изменение знака}
      {procedure _Min; {минимальное из двух}
      {procedure _Max; {максимальное из двух}
      {специальные функции}
      {procedure _Sin; {синус}
      {procedure _Cos; {косинус}
      {procedure _Exp; {экспонента}
      {procedure _Ln; {натуральный логарифм}
      {procedure _Lg; {десятичный логарифм}
      {операции сравнения}
      {procedure _Eq; {равно}
      {procedure _NE; {не равно}
      {procedure _LT; {меньше}
      {procedure _LE; {меньше или равно}
      {логические операции}
      {procedure _Not; {отрицание}
      {procedure _And; {конъюнкция}
      {procedure _Or; {дизъюнкция}
      {другие виды операций}
      {служебные методы}
    private
      {фиксация/обработка ошибок}
      procedure Failure(n:Word); {завершить аварийно}
      {вспомогательные функции}
    private
      {основные поля, обеспечивающие иерархию объектов
       конструированием}
      StType:CStackCh; {стек селекторов типа}
      StRe:CStackRe; {стек вещественных данных}
      StBo:CStackBo; {стек логических данных}
      {дополнительные и служебные поля}
    end;
  {другой вариант процессора или процессор селекторов типа}

Implementation
  Uses Crt;
  {$Q+}
  {в качестве примера реализованы только некоторые
   методы стекового процессора}
  constructor CPro.Create;
  begin
    StType.Create;
    StRe.Create;
    StBo.Create;
  end;
  destructor CPro.Destroy;
  begin
    StBo.Destroy; StRe.Destroy; StType.Destroy;
  end;
  procedure CPro.LoadB;
  begin
    StType.Push('B'); StBo.Push(b);
  end;
  procedure CPro.LoadR;
  begin
    StType.Push('R'); StRe.Push(x);
  end;
  procedure CPro.Del;
  {при реализации этого метода было бы удобнее использовать
   дополнительный метод стека - удаление}
  var t:Char; b:Boolean; x:_Real;
  begin
    if StType.IsEmpty then
      Failure(10)
    else
    begin
      StType.Pop(t);
      case t of
        'B': if StBo.IsEmpty then
               Failure(11)
             else
               StBo.Pop(b);
        'R': if StRe.IsEmpty then
               Failure(12)
             else
               StRe.Pop(x);
      else
        Failure(18);
      end; {case}
    end;
  end; {Del}
  procedure CPro.Top2Str;
  {при реализации этого метода было бы удобнее использовать
   дополнительный метод стека - "доступ к вершине стека для чтения"}
  var t:Char; b:Boolean; x:_Real;
  begin
    if StType.IsEmpty then
      Failure(20)
    else
    begin
      StType.Pop(t); StType.Push(t);
      case t of
        'B': if StBo.IsEmpty then
               Failure(21)
             else
             begin
               StBo.Pop(b); StBo.Push(b);
               if b then S:='истина' else S:='ложь';
             end;
        'R': if StRe.IsEmpty then
               Failure(22)
             else
             begin
               StRe.Pop(x); StRe.Push(x);
               str(x,S);
             end;
      else
        Failure(28);
      end; {case}
    end;
  end; {Top2Str}
  {procedure CPro._Power;
  {оба операнда типа _Real, преобразования не допускаются}
  {var t:Char; x,y,z:_Real; k:LongInt;
  begin
    if StType.IsEmpty then begin Failure(100); exit; end;
    {exit будет выполняться при замене фатального завершения
     фиксацией ошибки}
    {StType.Pop(t);
    if t <> 'R' then begin Failure(101); exit; end;
    if StRe.IsEmpty then begin Failure(102); exit; end;
    StRe.Pop(y); {получен показатель y}
    {if StType.IsEmpty then begin Failure(103); exit; end;
    StType.Pop(t);
    if t <> 'R' then begin Failure(104); exit; end;
    if StRe.IsEmpty then begin Failure(105); exit; end;
    StRe.Pop(x); {получено основание x}
    {if (frac(y)<>0) or (abs(y)>MaxLongInt) then
      if x>0 then z:=exp(y*ln(x))
      else begin Failure(106); exit; end;
    else
    begin {возвести в целую степень}
      {k:=round(y);
      if (k>0) or (x<>0) then
      begin
        z:=1.0;
        if k<0 then begin k:=-k; x:=1.0/x; end;
        while {инвариант: z*x**k} {k>0 do
        {begin
          if odd(k) then begin dec(k); z:=z*x; end
          else begin k:=k shr 1; x:=sqr(x); end;
        end;
      end
      else begin Failure(107); exit; end;
    end; {вычислен результат z}
    {StType.Push('R'); StRe.Push(z);
  end; {_Power}
  {procedure CPro._Eq;
  {оба операнда типа _Real, преобразования не допускаются}
  {var t:Char; x,y:_Real; b:Boolean;
  begin
    if StType.IsEmpty then begin Failure(300); exit; end;
    StType.Pop(t);
    if t <> 'R' then begin Failure(301); exit; end;
    if StRe.IsEmpty then begin Failure(302); exit; end;
    StRe.Pop(y); {получен второй операнд}
    {if StType.IsEmpty then begin Failure(303); exit; end;
    StType.Pop(t);
    if t <> 'R' then begin Failure(304); exit; end;
    if StRe.IsEmpty then begin Failure(305); exit; end;
    StRe.Pop(x); {получен первый операнд}
    {b:=(x=y); {результат b}
    {StType.Push('B'); StBool.Push(b);
  end; {_Eq}

  {function GetTopType:Char; {тип значения вершины}
  {begin
  end;}

  procedure CPro._Mult; {умножение}
   var t:TElemCh;
       x,y:TElemRe;

  begin
   if StType.IsEmpty then begin Failure(110); exit; end;
    StType.Pop(t);
   if t <> 'R' then begin Failure(111); exit; end;
    if StRe.IsEmpty then begin Failure(112); exit; end;
     StRe.Pop(y); {получен показатель y}
    if StType.IsEmpty then begin Failure(113); exit; end;
     StType.Pop(t);
    if t <> 'R' then begin Failure(114); exit; end;
    if StRe.IsEmpty then begin Failure(115); exit; end;
     StRe.Pop(x);
    x:=x*y;
    StRe.Push(x);
    StType.Push('R');
  end;

  procedure CPro._Div; {деление}
   var t:TElemCh;
      x,y:TElemRe;

 begin
  if StType.IsEmpty then begin Failure(120); exit; end;
   StType.Pop(t);
  if t <> 'R' then begin Failure(121); exit; end;
   if StRe.IsEmpty then begin Failure(122); exit; end;
    StRe.Pop(y); {получен показатель y}
   if StType.IsEmpty then begin Failure(123); exit; end;
    StType.Pop(t);
   if t <> 'R' then begin Failure(124); exit; end;
   if StRe.IsEmpty then begin Failure(125); exit; end;
    StRe.Pop(x);
   if y=0 then begin Failure(126); exit; end;
   x:=x/y;
   StRe.Push(x);
   StType.Push('R');
 end;

  procedure CPro._Add; {сложение}
   var t:TElemCh;
       x,y:TElemRe;

  begin
   if StType.IsEmpty then begin Failure(130); exit; end;
    StType.Pop(t);
   if t <> 'R' then begin Failure(131); exit; end;
    if StRe.IsEmpty then begin Failure(132); exit; end;
     StRe.Pop(y); {получен показатель y}
    if StType.IsEmpty then begin Failure(133); exit; end;
     StType.Pop(t);
    if t <> 'R' then begin Failure(134); exit; end;
    if StRe.IsEmpty then begin Failure(135); exit; end;
     StRe.Pop(x);
    x:=x+y;
    StRe.Push(x);
    StType.Push('R');
  end;

  procedure CPro._Sub; {вычитание}
   var t:TElemCh;
       x,y:TElemRe;

  begin
   if StType.IsEmpty then begin Failure(140); exit; end;
    StType.Pop(t);
   if t <> 'R' then begin Failure(141); exit; end;
    if StRe.IsEmpty then begin Failure(142); exit; end;
     StRe.Pop(y); {получен показатель y}
    if StType.IsEmpty then begin Failure(143); exit; end;
     StType.Pop(t);
    if t <> 'R' then begin Failure(144); exit; end;
    if StRe.IsEmpty then begin Failure(145); exit; end;
     StRe.Pop(x);
    x:=x-y;
    StRe.Push(x);
    StType.Push('R');
  end;
  procedure CPro._Inv; {изменение знака}
   var t:TElemCh;
       x:TElemRe;

  begin
   if StType.IsEmpty then begin Failure(150); exit; end;
    StType.Pop(t);
    if t <> 'R' then begin Failure(151); exit; end;
    if StRe.IsEmpty then begin Failure(152); exit; end;
     StRe.Pop(x);
    x:=-x;
    StRe.Push(x);
    StType.Push('R');
  end;

  {фиксация/обработка ошибок процессора}
  procedure CPro.Failure;
  begin
    writeln; writeln('Ошибка CPro # ',n:1);
    case n of
      10..18: begin
                write('Метод Del: ');
                case n of
                  10    : writeln('стек пуст');
                  11..17: writeln('разрушена целостность стека');
                  18    : writeln('недопустимый селектор типа');
                end;
              end;
      20..28: begin
                write('Метод Top2Str: ');
                case n of
                  20    : writeln('стек пуст');
                  21..27: writeln('разрушена целостность стека');
                  28    : writeln('недопустимый селектор типа');
                end;
              end;
    100..107: begin
                write('Метод _Power: ');
                case n of
                  100    : writeln('нет 2-го операнда операции');
                  101,104: writeln('неверный тип операнда или',
                                   ' недопустимый селектор типов');
                  102,105: writeln('разрушена целостность стека');
                  103    : writeln('нет 1-го операнда операции');
                  106,107: writeln('недопустимые значения операндов');
                end;
              end;
    300..305: begin
                write('Метод _Eq: ');
                case n of
                  300    : writeln('нет 2-го операнда операции');
                  301,304: writeln('неверный тип операнда или',
                                   ' недопустимый селектор типов');
                  302,305: writeln('разрушена целостность стека');
                  303    : writeln('нет 1-го операнда операции');
                end;
              end;
    end;
    Halt(1); {выход в операционную среду}
  end; {Failure}
end.

