program x4_z5;
uses crt;
var c: char; s: string;
begin
    clrscr;
    
    repeat
        write('Введите символ: ');
        {$I-} readln(c); {$I+}
    until IORESULT=0;

    case c of
        '0'..'9': s := 'Цифра';
        'A'..'Z', 'a'..'z': s := 'Латинская буква';
        //'а'..'я', 'А'..'Я', 'ё', 'Ё': s := 'Русская буква';
        '.',',',';','!','?': s := 'Знак препинания'
        else s := 'Другой символ';
    end;

    writeln(s);
    readkey;
end.
