program zz3;
uses crt;
var x1, x2, x3, y1, y2, y3, tmp, a, b, c, t: single;
begin
    clrscr;
    
    write('Введите A(x, y): ');
    readln(x1, y1);

    write('Введите B(x, y): ');
    readln(x2, y2);

    write('Введите C(x, y): ');
    readln(x3, y3);

    a := sqr(x2-x1) + sqr(y2-y1);
    c := sqr(x3-x2) + sqr(y3-y2);
    b := sqr(x3-x1) + sqr(y3-y1);

    if b > a then begin
        t := b;
        b := a;
        a := t;
    end else if c > a then begin
        t := c;
        c := a;
        a := t;
    end else begin
        t := c;
        c := b;
        b := t;
    end;

    tmp := a + b;

    if tmp = c
        then writeln('Прямоугольный')
    else if tmp > c
        then writeln('Тупоугольный')
        else writeln('Остроугольный');

    readkey;
end.
