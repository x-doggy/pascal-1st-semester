program zz2;
uses crt;
var a, b, c: single;
begin
    clrscr;
    
    repeat
        write('Введите длины треугольника: ');
        readln(a, b, c);
    until (a>0) and (b>0) and (c>0);
    
    if (a+b < c) or (a+c < b) or (b+c < a)
        then writeln('Да, существует!')
        else writeln('Нет, не существует!');
    
    readkey;
end.
