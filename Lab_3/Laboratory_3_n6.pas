program z6;
uses crt;
var n, d: word; x: extended;
begin
    clrscr;
    
    repeat
        write('Введите n: ');
        readln(n);
    until (n mod 2 <> 0) and (n>1);

    d := 3;
    x := 0;
    
    while n >= d do begin
        x := x + 1 / sqrt((d-2) * d);
        d := d+2;
    end;
    
    writeln(x:10:5);
    readkey;
end.
