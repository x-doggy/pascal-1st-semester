program z7;
uses crt;
var i: integer; t: string;
begin
    clrscr;

    repeat
        write('Введите имя: ');
        readln(t);
    until length(t)<=62;

    for i:=1 to length(t)+17 do write('*');
    writeln;
    writeln('* Доброе утро, ', t, ' *');

    for i:=1 to length(t)+17 do write('*');
    writeln;

    readkey;
end.
