program zz1;
uses crt;
var a, b, c: single;
begin
    clrscr;

    write('Введите три числа (a, b, c): ');
    readln(a, b, c);

    if (a > 0) and (b > 0) and (c > 0)
        then writeln('Все числа положительные!')
        else writeln('Не все числа положительные!');

    readkey;
end.
