program Ind2;
uses crt;
{$R-}
const N=500; M=500;
type
     TElem=single;
     TArr=array[1..1] of TElem;
     PArr=^TArr;
     TMas=array[1..1] of PArr;
     PMas=^TMas;
     FSingle=file of single;
     FText=text;
     TTen=0..10;
var
     a: PMas;
     width, height, res, i, j: word;
     act: TTen;
     inp, oup: FSingle;
     err: boolean;
     need1, need2: longint;
     filename: string;

function BoolToStr(const value: boolean): string;
(*Функция возвращает строку 'Yes' для True или 'No' для False*)
const Values: array[boolean] of string = ('No', 'Yes');
begin
   BoolToStr := Values[value];
end;

//2
procedure Spiral(var a: PMas; x, y: word; var res: word; var ce: boolean);

(*
* Процедура считает сколько раз за нулём следует положительное число,
* если обходить массив по спирали против часовой стрелки

Входные параметры:
@ a         Указатель на массив;
@ x, y      Число строк и столбцов массива;

Выходные параметры:
@ res       Результат выполнения алгоритма;
@ ce        Код выхода.
            Возвращает False, если размеры массива превышают 500
            или если массив вовсе не создан,
            иначе возвращает True;
*)

var i, j, k, nz: integer;
begin
    
    if (x>N) or (y>M) or (a=NIL) then ce := false else begin
        i:=0;
        j:=0;
        nz := (x+y) div 2-1;
        res := 0;

        if (x mod 2 <> 0) or (y mod 2 <> 0) then dec(nz);

        for k:=nz downto 1 do begin
            i:=k; j:=k-1;
            while i>=1 do begin
                if (i<k) and (a^[i+1]^[j]=0) and (a^[i]^[j] > 0) then inc(res);
                dec(i);
            end;

            i:=k;
            while j>=1 do begin
                if (j<k) and (a^[i]^[j+1]=0) and (a^[i]^[j] > 0) then inc(res);
                dec(j);
            end;

            i:=1; j:=1;
            while i<=k do begin
                if (i>1) and (a^[i-1]^[j]=0) and (a^[i]^[j] > 0) then inc(res);
                inc(i);
            end;

            i:=k;
            while j<=k do begin
                if (j>1) and (a^[i]^[j-1]=0) and (a^[i]^[j] > 0) then inc(res);
                inc(j);
            end;

        end; // for
        ce := true;
    end; // if
end; // procedure

begin {main}

    textbackground(blue);
    textcolor(yellow);

    a := NIL;
    width := 0;
    height := 0;
    need1 := 0;
    need2 := 0;

    repeat
        clrscr;

        writeln;
        writeln('    (c) 2013, Владимир Стадник, 2-я прога.');
        writeln('  Программа сделана в Linux Mint 15 KDE 64bit');
        writeln;
        writeln('1. Определить размеры массива');
        writeln('2. Заполнить массив с клавиатуры');
        writeln('3. Заполнить массив случайным образом');
        writeln('4. Заполнить массив из файла');
        writeln('5. Просмотреть массив');
        writeln('6. >>>> Выполнить алгоритм <<<<');
        writeln('8. Выйти из программы');
        writeln;
        write('Введите номер действия: ');
        repeat
            {$I-} readln(act); {$I+}
        until (IORESULT=0) and ((act>=1) and (act<=6) or (act=8));

        case act of

            1:
            begin
                writeln('1. <Определение размеры массива>');
                if a<>NIL then begin
                    for i:=1 to width do FreeMem(a^[i], need2);
                    FreeMem(a, need1);
                    a := NIL;
                end;

                write('Введите ширину массива: ');
                readln(width);
                write('Введите высоту массива: ');
                readln(height);

                need1 := longint(width) * longint(height) * sizeof(PArr);
                need2 := longint(height) * sizeof(TElem);
                GetMem(a, need1);
                for i:=1 to width do GetMem(a^[i], need2);
            end;

            2:
            begin
                writeln('2. <Заполнение с клавиатуры>');
                for i:=1 to width do
                    for j:=1 to height do begin
                        write('A[', i, ', ', j, '] = ');
                        read(a^[i]^[j]);
                        writeln('OK!');
                    end;
            end;

            3:
            begin
                writeln('3. <Заполнение случайно>');
                Randomize;
                write('Введите имя файла для сохранения: ');
                readln(filename);

                Assign(oup, filename);
                {$I-} Rewrite(oup); {$I+}
                if IOResult = 0 then begin

                    for i:=1 to width do begin
                        for j:=1 to height do begin
                            a^[i]^[j] := random(10) - 5;
                            write(a^[i]^[j]:8:2);
                            write(oup, a^[i]^[j]);
                        end;
                        writeln;
                    end;
                end;

                close(oup);
                filename := '';
            end;

            4:
            begin
                writeln('4. <Заполнение из файла>');
                if a <> NIL then begin
                    for i:=1 to width do FreeMem(a^[i], need2);
                    FreeMem(a, need1);
                    a := NIL;
                end;
                write('Введите имя файла: ');
                readln(filename);

                Assign(inp, filename);
                {$I-} Reset(inp); {$I+}
                if IOResult = 0 then begin
                    writeln('Ширина = ', width, '; Высота = ', height);

                    need1 := longint(width) * longint(height) * sizeof(PArr);
                    need2 := longint(height) * sizeof(TElem);
                    GetMem(A, need1);
                    for i:=1 to width do GetMem(a^[i], need2);

                    for i:=1 to width do begin
                        for j:=1 to height do begin
                            {$I-} read(inp, a^[i]^[j]); {$I+}
                            if IOResult = 0 then write(a^[i]^[j]:8:2);
                        end; // while [j]
                        writeln;
                    end; // while [i]
                end; // if

                {$I-} close(inp); {$I+}
                filename := '';
            end;

            5:
            begin
                writeln('5. <Просмотр массива>');
                for i:=1 to width do begin
                    for j:=1 to height do write(a^[i]^[j]:8:2);
                    writeln;
                end;
            end;

            6:
            begin
                writeln('6. <Выполнение алгоритма>');
                Spiral(a, width, height, res, err);
                writeln('Программа выполнена верно? - ', BoolToStr(err));
                writeln('Результат = ', res);
            end;

        end; //case
        if act<>8 then readkey;
    until act=8; //repeat

    writeln('Очистка памяти...');
    for i:=1 to width do FreeMem(a^[i], need2);
    FreeMem(a, need1);
    a := NIL;
    {$R+}
    writeln('Выполнено! Нажмите [Enter] для выхода');

    readln;
end.
