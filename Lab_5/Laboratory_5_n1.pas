uses crt;
type TElem = integer;
     TFile = file of TElem;
var  f: TFile;
     x: TElem;
     n, i: integer;
begin {main}
     textbackground(red);
     textcolor(yellow);
     clrscr;

     {$I-}
     assign(f, 'f1.dat');
     rewrite(f);
     {$I+}
     if IORESULT<>0 then writeln('Ошибка перезаписи файла') else begin
        write('Введите кол-во чисел: ');
        readln(n);

        randomize;
        for i:=1 to n do begin
            x := random(10);
            write(f, x);
        end;

        close(f);
     end;
     readkey;
end.
