uses crt;
type TElem = integer;
     TFile = file of TElem;
     TMas  = array[1..20, 1..20] of TElem;
var  f: TFile;
     x: TElem;
     a: TMas;
     m, n, i, j: integer;
     ok: boolean;

begin {main}
     textbackground(red);
     textcolor(yellow);
     clrscr;

     Assign(f, 'f2.dat');
     {$I-} Reset(f); {$I+}
     if IORESULT<>0 then writeln('Нельзя прочитать файл!') else begin
        {$I-} read(f, m, n); {$I+}
        if IORESULT<>0 then writeln('Размеры не прочитаны!') else begin
            randomize;
            for i:=1 to m do begin
                for j:=1 to n do begin
                    {$I-} read(f, a[i, j]); {$I+}
                    if IOResult<>0 then ok := false;
                end;
            end;

            writeln;
            writeln('Вывод массива:');

            if ok then begin
                for i:=1 to m do begin
                    for j:=1 to n do write( a[i, j]:4 );
                    writeln;
                end;
            end else writeln('Произошла ошибка!');
        end;
    end;
    {$I-} close(f); {$I+}
    readkey;
end.
