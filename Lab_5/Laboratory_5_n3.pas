uses crt;
{$R-}
type TElem=integer;
     TArr=array[1..1] of TElem;
     PArr=^TArr;
     TMas=array[1..1] of PArr;
     PMas=^TMas;
var  a: PMas;
     b: PArr;
     i, j, n, m: integer;
     need1, need2: longint;

procedure SwapRows(var m: PMas; s1, s2: TElem);
var tmp: PArr;
begin
     tmp := m^[s1];
     m^[s1] := m^[s2];
     m^[s2] := tmp;
end;

begin
     textbackground(red);
     textcolor(yellow);
     clrscr;

     write('Введите размеры таблицы: ');
     readln(n, m);

     need1 := longint(n) * longint(m) * sizeof(PArr);
     need2 := longint(m) * sizeof(TElem);

     GetMem(a, need1);
     for i:=1 to n do GetMem(a^[i], need2);

     Randomize;
     for i:=1 to n do begin
        for j:=1 to m do begin
            a^[i]^[j] := random(50)-25;
            write(a^[i]^[j]:3, '  ');
        end;
        writeln;
     end;

     writeln;
     SwapRows(a, 1, 2);
     writeln('После перестановки:');

     for i:=1 to n do begin
        for j:=1 to m do
            write(a^[i]^[j]:3, '  ');
        writeln;
     end;

     for i:=1 to n do FreeMem(a^[i], need2);
     FreeMem(a, need1);
     {$R+}
     readkey;
end.
