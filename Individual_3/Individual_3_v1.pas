program __z_pr3;
{$R-}
uses crt;
type
    TElem   = single;
    TArr    = array[1..1] of TElem;
    PArr    = ^TArr;
    TMas    = array[1..1] of PArr;
    PMas    = ^TMas;
    FSingle = file of single;
var
    a: PMas;
    w, h, i, j, act: byte;
    r: word;
    inp, oup: FSingle;
    e: boolean;
    need1, need2: longint;

procedure Def_Sizes(var z: PMas; var width, height: byte; var e: boolean);
var k: byte; n1, n2: longint;
begin
    e := false;

    if (z <> NIL) and (n1 <> 0) and (n2 <> 0) then begin
        for k:=1 to width do FreeMem(z^[k], n2);
        FreeMem(z, n1);
        z := NIL;
    end;

    write('Введите число строк матрицы: ');
    readln(width);
    write('Введите число столбцов матрицы: ');
    readln(height);

    n1 := longint(width) * longint(height) * sizeof(PArr);
    n2 := longint(height) * sizeof(TElem);
    GetMem(z, n1);
    for k:=1 to width do GetMem(z^[k], n2);

    e := true;
end;

procedure Fill_Kbd(var z: PMas; width, height: byte; var e: boolean);
var k, l: byte;
begin
    if (z=NIL) or (width=0) or (height=0) then e := false
    else begin
        for k:=1 to width do
            for l:=1 to height do begin
                write('z[', k, ', ', l, '] = ');
                readln(z^[k]^[l]);
                if IOResult=0 then write('OK!') else e := false;
            end;
        e := true;
    end;
end;

procedure Fill_Rnd(var z: PMas; width, height: byte; var e: boolean);
var k, l: byte;
begin
    if (z=NIL) or (width=0) or (height=0) then e := false
    else begin
        Randomize;
        for k:=1 to width do
            for l:=1 to height do
                z^[k]^[l] := random(10) - 5;
        e := true;
    end;
end;

begin {main}

    textbackground(black);
    textcolor(white);

    a     := NIL;
    need1 := 0;
    need2 := 0;

    repeat

        clrscr;

        // Организация меню
        writeln;
        writeln('          (c) 2013, Владимир Стадник, 3-я прога.');
        writeln('      Программа была создана в Windows XP SP3 32bit');
        writeln(' Исходник будет доступен под условиями лицензии CC BY 3.0');
        writeln;
        writeln('1. Определить размеры таблицы');
        writeln('2. Заполнить таблицу с клавиатуры');
        writeln('3. Заполнить таблицу случайно');
        writeln('4. Заполнить таблицу из файла');
        writeln('5. Вывести таблицу на экран');
        writeln('6. Выполнить подпрограмму-алгоритм № 16');
        writeln('8. Выйти из программы');
        writeln;
        write('Введите номер нужного действия: ');
        readln(act);

        // Обработка ошибки введённого номера
        while not act in [1..6, 8] do begin
            writeln('Неверно введён номер действия!');
            write('Введите номер нужного действия ещё раз: ');
            readln(act);
        end;

        // Пункты меню
        case act of

            1:
            begin
                writeln('(1)');
                Def_Sizes(a, w, h, e);
                if not e
                    then writeln('Ошибка: массив уже создан!')
                    else writeln('OK!');
            end;

            2:
            begin
                writeln('(2)');
                Fill_Kbd(a, w, h, e);
                if not e
                    then writeln('Ошибка ввода с клавиатуры!')
                    else writeln('OK!');
            end;

            3:
            begin
                writeln('(3)');
                Fill_Rnd(a, w, h, e);
                if not e
                    then writeln('Ошибка ДСЧ!')
                    else writeln('OK!');
            end;

        end;

    until act=8;

end.
