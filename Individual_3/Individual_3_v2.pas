program __z_pr3;
{$R-}
uses crt;
type
     TElem    = single;
     TArr     = array[1..1] of TElem;
     PArr     = ^TArr;
     TMas     = array[1..1] of PArr;
     PMas     = ^TMas;
     FSingle  = file of single;
     FText    = text;
     TTen     = 0..10;
var
     a:  PMas;
     width, height, i, j, index1, index2: word;
     w, h, res, elem: TElem;
     act: TTen;
     inp, oup: FSingle;
     err, ok: boolean;
     need1, need2: longint;
     filename: string;

function BoolToStr(const value: boolean): string;
(* Фунукция возвращает строковое представление логической переменной
   Входная константа: логическая переменная для преобразования
   Выходной поток: строковое написание значения логической переменной *)

const Values: array[boolean] of string = ('Нет', 'Да');
begin
   BoolToStr := Values[value];
end;

// 16
procedure Prg16(var z: PMas; w, h: word; var ind1, ind2: word;
                var el: TElem; var r: TElem; var ce: boolean);
(* Функция находит значение и положение в таблице элемента,
   для которого сумма элементов, лежащих не выше и не левее его, максимальна.
    Входные параметры:
    z          - указатель на массив для проверки
    w, h       - размеры массива z
    Выходные параметры:
    ind1, ind2 - индексы, о которых говорится в задании
    el         - элемент, для которого определена максимальная сумма
    r          - максимальная сумма, лежащая не выше и не левее el
    ce         - код завершения: False, если массив передался неверно,
                 True в противном случае, когда успешно.
*)
var i, j, k, l: integer; s, m: TElem;
begin
    if (z=NIL) then begin
        ce := false;
    end else begin
        m := z^[1]^[1];
        for i:=1 to w do begin
            for j:=2 to h do begin
                r := 0;
                for k:=i to w do
                    for l:=j to h do
                        r := r + z^[k]^[l];
                if res>m then begin
                    m    := r;
                    ind1 := i;
                    ind2 := j;
                    el   := z^[i]^[j];
                end;
            end;
        end;
        ce := true;
    end;
end;

begin {main}

    textbackground(blue);
    textcolor(yellow);

    repeat
        clrscr;

        writeln;
        writeln('        © 2013 Владимир Стадник, 3-я прога');
        writeln('  Программа создана в GNU/Linux Mint 15 KDE 64bit');
        writeln(' Исходный код будет доступен под лицензией CC BY 3.0');
        writeln;
        writeln('1. Определить размеры таблицы');
        writeln('2. Заполнить таблицу с клавиатуры');
        writeln('3. Заполнить таблицу псевдослучайно');
        writeln('4. Заполнить таблицу из файла');
        writeln('5. Вывести массив на экран');
        writeln('6. Вызвать подпрограмму № 16');
        writeln('7. Записать массив в файл (только после 1 и 3)');
        writeln('8. Выйти из программы');
        writeln;
        write('Введите номер необходимого действия: ');
        readln(act);

        while not act in [1..8] do begin
            writeln('Неверное число-действие!');
            write('Введите номер действия снова: ');
            readln(act);
        end;
        
        case act of

            1:
            begin
                writeln('Определение размеров таблицы');

                if a<>NIL then begin
                    for i:=1 to width do FreeMem(pointer(a^[i]), need2);
                    FreeMem(pointer(a), need1);
                    a := NIL;
                end;

                write('Ширина таблицы: ');
                readln(width);
                write('Высота таблицы: ');
                readln(height);

                need1 := longint(width) * longint(height) * sizeof(PArr);
                need2 := longint(height) * sizeof(TElem);
                GetMem(pointer(a), need1);
                for i:=1 to width do GetMem(pointer(a^[i]), need2);
            end;

            2:
            begin
                writeln('Заполнение таблицы с клавиатуры');
                for i:=1 to width do
                    for j:=1 to height do begin
                        write('A[', i, ', ', j, '] = ');
                        read(a^[i]^[j]);
                        writeln('OK!');
                    end;
            end;

            3:
            begin
                writeln('Заполнение таблицы псевдослучайными числами');
                Randomize;
                    for i:=1 to width do begin
                        for j:=1 to height do begin
                            a^[i]^[j] := random(50) - 25;
                            write(a^[i]^[j]:8:2);
                        end;
                        writeln;
                    end;
            end;

            4:
            begin
                writeln('Заполнение таблицы из файла');

                if a<>NIL then begin
                    for i:=1 to width do FreeMem(pointer(a^[i]), need2);
                    FreeMem(pointer(a), need1);
                    a := NIL;
                end;

                write('Введите имя прочитываемого файла: ');
                readln(filename);

                Assign(inp, filename);
                {$I-} Reset(inp); {$I+}
                if IOResult=0 then begin
                    writeln('Ширина=', width, '; Высота=', height);

                    need1 := longint(width) * longint(height) * sizeof(PArr);
                    need2 := longint(height) * sizeof(TElem);
                    GetMem(pointer(a), need1);
                    for i:=1 to width do GetMem(pointer(a^[i]), need2);

                    for i:=1 to width do begin
                        for j:=1 to height do begin
                            read(inp, a^[i]^[j]);
                            if IOResult=0
                                then write(a^[i]^[j]:8:2)
                                else begin
                                    writeln;
                                    writeln('Ошибка чтения файла');
                                    ok := false;
                                end;
                        end; // for j
                        writeln;
                    end; // for i
                end else
                    writeln('Ошибка связывания файла: файла не существует!'); // if
                seek(inp, 0);
                close(inp);
                //erase(inp);
                filename := '';
            end;

            5:
            begin
                writeln('Вывод массива');
                for i:=1 to width do begin
                    for j:=1 to height do write(a^[i]^[j]:8:2);
                    writeln;
                end;
            end;

            6:
            begin
                writeln('Вызов подпрограммы...');
                Prg16(a, width, height, index1, index2, elem, res, err);
                writeln('Программа завершилась успешно? - ', BoolToStr(err));
                writeln('Результат  =     (', index1, '; ', index2, ')');
                writeln('Элемент    = ', elem:8:2);
                writeln('Сумма      = ', res:8:2);
            end;

            7:
            begin
                writeln('Запись массива в файл');
                write('Введите имя перезаписываемого файла: ');
                readln(filename);

                Assign(oup, filename);
                {$I-}Rewrite(oup);{$I+}
                if IOResult=0 then begin
                    for i:=1 to width do begin
                        for j:=1 to height do write(oup, a^[i]^[j]);
                        writeln;
                    end;
                end else begin
                    writeln;
                    writeln('Ошибка связывания файла: файла не существует!');
                    act := 8;
                    break;
                end;

                close(oup);
            end;

        end; //case
        if act<>8 then readkey;
    until act=8; //repeat

    writeln('Спасибо за использование программы!');
    writeln('Теперь программа будет закрыта.');

    writeln('Очистка памяти...');
    for i:=1 to width do FreeMem(pointer(a^[i]), need2);
    FreeMem(pointer(a), need1);
    a := NIL;
    {$R+}
    writeln('Выполнено! Нажмите [Enter] для выхода');

    readln;
end.
