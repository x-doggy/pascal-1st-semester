program __z_pr3;
// Encoding UTF-8
uses crt;
const max     = 25;
type
     TElem    = single;
     TArr     = array[1..1] of TElem;
     PArr     = ^TArr;
     TMas     = array[1..1] of PArr;
     PMas     = ^TMas;
     TPnt     = array[1..max] of PMas;
     PPnt     = ^TPnt;
     FSingle  = file of single;
     FText    = text;
     TTen     = 0..12;
var
     a:  PPnt;
     width, height, amount, i, j, k, index1, index2: word;
     w, h, res, elem: TElem;
     act: TTen;
     inp, oup: FSingle;
     err, ok: boolean;
     need1, need2, need3: longint;
     filename: string;

function BoolToStr(const value: boolean): string;
(* Фунукция возвращает строковое представление логической переменной
   Входная константа: логическая переменная для преобразования
   Выходной поток: строковое написание значения логической переменной *)

const Values: array[boolean] of string = ('Нет', 'Да');
begin
   BoolToStr := Values[value];
end;

// 16
procedure Prg16(var z: PPnt; k, amount, w, h: word; var ind1, ind2: word;
                var el: TElem; var r: TElem; var ce: boolean);
(* Функция находит значение и положение в таблице элемента,
   для которого сумма элементов, лежащих не выше и не левее его, максимальна.
    Входные параметры:
    z          - указатель на массив для проверки
    w, h       - размеры массива z
    Выходные параметры:
    ind1, ind2 - индексы, о которых говорится в задании
    el         - элемент, для которого определена максимальная сумма
    r          - максимальная сумма, лежащая не выше и не левее el
    ce         - код завершения: False, если массив передался неверно,
                 True в противном случае, когда успешно.*)
var i, j, d, l: integer; s, m: TElem;
begin
    {$R-}
    if (z=NIL) then begin
        ce := false;
    end else begin
        m := z^[k]^[1]^[1];
        for i:=1 to w do begin
            for j:=2 to h do begin
                r := 0;
                for d:=i to w do
                    for l:=j to h do
                        r := r + z^[k]^[d]^[l];
                if res>m then begin
                    m    := r;
                    ind1 := i;
                    ind2 := j;
                    el   := z^[k]^[i]^[j];
                end;
            end;
        end;
        ce := true;
    end;
end;

procedure get_memory(var z: PPnt; k, amount, w, h: word; var n1, n2, n3: longint; var ce: boolean);
{Процедура выделяет память для массива z необходимое кол-во памяти в соответствии с ее размерами.
Если массив уже был создан, то программа завершает работу, предварительно вернув код завершения ce=false.
На вход подпрограмма принимает динамический массив, для которого будет выделяться память;
Параметры w, h - ширина/высота таблицы - небходимы для вычисления нужного кол-ва памяти.
В параметры n1, n2 передаются по ссылке чтобы была сохранена инф-ция о том,
  сколько потом нужно очистить памяти для корректного завершения программы.
Логический параметр ce передаёт код завершения: true, если массив ранее не был создан
  и было произведено выделение необходимого кол-ва памяти; false в противном случае}
var d: integer;
begin
        if z=NIL then begin
                {n1 := longint(w) * longint(h) * sizeof(PArr);
                n2 := longint(h) * sizeof(TElem);
                GetMem(pointer(z), n1);
                for d:=1 to w do GetMem(pointer(z^[d]), n2);}
                ce := true;

                n1 := longint(k) * longint(w) * longint(h) * sizeof(PMas);
                n2 := longint(w) * longint(h) * sizeof(PArr);
                n3 := longint(h) * sizeof(TElem);

                getmem(pointer(z), n1);
                for i:=1 to amount do begin
                    getmem(pointer(z^[i]), n2);
                    for j:=1 to w do getmem(pointer(z^[i]^[j]), n3);
                end;
        end else ce := false;
end;

procedure clear_memory(var z: PPnt; k, amount, w: word; var n1, n2, n3: longint; var ce: boolean);
{
Процедура очищает память для массива z.
Если массив не был создан, то программа завершает работу, предварительно вернув код завершения ce=false.
На вход подпрограмма принимает динамический массив, для которого будет очищена память;
Параметры w - ширина таблицы - небходима для вычисления нужного кол-ва памяти.
В параметры n1, n2 передаются по ссылке чтобы была сохранена инф-ция о том,
  сколько потом нужно очистить памяти для корректного завершения программы.
Логический параметр ce передаёт код завершения:
  true, если массив ранее был создан; false в противном случае
}
var d: integer;
begin
        if (z<>NIL) and (w>0) and (n1>0) and (n2>0) then begin
                for i:=1 to k do begin
                    for j:=1 to w do freemem(pointer(z^[i]^[j]), n3);
                    freemem(pointer(z^[i]), n2);
                end;
                freemem(pointer(z), n1);
                z := NIL;
                ce := true;
        end else ce := false;
end;

procedure read_file(var z: PPnt; var f: FSingle; fname: string; k, amount, w, h: word; var cerr: boolean);
var need1, need2, need3: longint; p, q: word;
begin
    cerr := not (fname='');
    if cerr then begin
        assign(f, fname);
        {$I-} reset(f); {$I+}
        cerr := IOResult<>0;
        if not cerr then begin
            get_memory(z, k, amount, w, h, need1, need2, need3, cerr);
            if cerr then begin
                {$I-} seek(f, 0); {$I+}
                for p:=1 to w do
                    for q:=1 to h do read(f, z^[k]^[p]^[q]);
            end;
        end;
        {$I-} seek(f, 0); {$I+}
        {$I-}  close(f);  {$I+}
    end;
end;

procedure write_file(var z: PPnt; var f: FSingle; fname: string; k, amount, w, h: word; var cerr: boolean);
var need1, need2, need3: longint; p, q: word;
begin
    cerr := not (fname='');
    if cerr then begin
        assign(f, fname);
        {$I-} rewrite(f); {$I+}get_memory(a, k, amount, width, height, need1, need2, need3, err);
                if not err then act := 11;
        cerr := IOResult<>0;
        if not cerr then begin
            get_memory(z, k, amount, w, h, need1, need2, need3, cerr);
            if cerr then begin
                {$I-} seek(f, 0); {$I+}
                for p:=1 to w do
                    for q:=1 to h do write(f, z^[k]^[p]^[q]);
            end;
        end;
        {$I-} seek(f, 0); {$I+}
        {$I-}  close(f);  {$I+}
    end;
end;

begin {main}

    textbackground(blue);
    textcolor(yellow);

    need1 := 0;
    need2 := 0;
    need3 := 0;
    k     := 1;
    amount:= 1;
    a     := NIL;
    err   := true;

    {$R-}

    repeat
        clrscr;

        writeln;
        writeln('         © 2013 Владимир Стадник, 3-я прога');
        writeln('      Программа создана в Kubuntu 13.10 64bit');
        writeln(' Исходный код будет доступен под лицензией CC BY 3.0');
        writeln;
        writeln('Текущая таблица: ', k, '; Всего таблиц: ', amount);
        writeln;
        writeln(' 1. Определить размеры таблицы');
        writeln(' 2. Заполнить таблицу с клавиатуры');
        writeln(' 3. Заполнить таблицу псевдослучайно');
        writeln(' 4. Заполнить таблицу из файла');
        writeln(' 5. Вывести массив на экран');
        writeln(' 6. Вызвать подпрограмму № 16');
        writeln(' 7. Записать массив в файл (только после 1 и 3)');
        writeln(' 8. Сменить массив');
        writeln('11. Выйти из программы');
        writeln;
        write('Введите номер необходимого действия: ');
        readln(act);

        while not act in [1..11] do begin
            writeln('Неверное число-действие!');
            write('Введите номер действия снова: ');
            readln(act);
        end;

        case act of

            1:
            begin
                writeln('Определение размеров таблицы');

                amount := amount + 1;

                write('Ширина таблицы: ');
                readln(width);
                write('Высота таблицы: ');
                readln(height);

                get_memory(a, k, amount, width, height, need1, need2, need3, err);
                if not err then act := 11;
            end;

            2:
            begin
                writeln('Заполнение таблицы с клавиатуры');
                for i:=1 to width do
                    for j:=1 to height do begin
                        write('a[', k, ', ', i, ', ', j, '] = ');
                        read(a^[k]^[i]^[j]);
                        writeln('OK!');
                    end;
            end;

            3:
            begin
                writeln('Заполнение таблицы псевдослучайными числами');
                Randomize;
                    for i:=1 to width do begin
                        for j:=1 to height do begin
                            a^[k]^[i]^[j] := random(50) - 25;
                            write(a^[k]^[i]^[j]:8:2);
                        end;
                        writeln;
                    end;
            end;

            4:
            begin
                writeln('Заполнение таблицы из файла');

                (*if a<>NIL then begin
                    clear_memory(a, k, amount, width, need1, need2, need3, err);
                    if not err then act := 8;
                end;*)

                write('Введите имя прочитываемого файла: ');
                readln(filename);

                read_file(a, inp, filename, k, amount, width, height, err);

                write('Файл ''', filename, ''' ');
                if not err then write('не ');
                writeln('был успешно прочитан');
            end;

            5:
            begin
                writeln('Вывод массива');
                if a<>NIL then begin
                     for i:=1 to width do begin
                         for j:=1 to height do write(a^[k]^[i]^[j]:8:2);
                     writeln;
                     end;
                end else writeln('');
            end;

            6:
            begin
                writeln('Вызов подпрограммы...');
                Prg16(a, k, amount, width, height, index1, index2, elem, res, err);
                writeln('Программа завершилась успешно? - ', BoolToStr(err));
                writeln('Результат  =     (', index1, '; ', index2, ')');
                writeln('Элемент    = ', elem:8:2);
                writeln('Сумма      = ', res:8:2);
            end;

            7:
            begin
                writeln('Запись массива в файл');

                {if a<>NIL then begin
                    clear_memory(a, k, amount, width, need1, need2, need3, err);
                    if not err then act := 11;
                end;}

                write('Введите имя перезаписываемого файла: ');
                readln(filename);

                write_file(a, inp, filename, k, amount, width, height, err);

                write('Файл ''', filename, ''' ');
                if not err then write('не ');
                writeln('был успешно записан');
            end;

            8:
            begin
                writeln('Переключение на др. массив');
                write('Какой по счёту массив вы хотите использовать? > ');
                readln(k);
            end;


        end; //case

        err      := true;
        filename := '';

        if act<>11 then readkey;
    until act=11; //repeat

    writeln('Спасибо за использование программы!');
    writeln('Теперь программа будет закрыта.');

    writeln('Очистка памяти...');
    clear_memory(a, k, amount, width, need1, need2, need3, err);
    {$R+}
    writeln('Выполнено! Нажмите [Enter] для выхода');

    readln;
end.
