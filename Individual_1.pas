program Individual_1;
uses crt;

const a=-2.7; b=2.3;                 (* Границы табулирования *)
      h=0.025;                       (* Шаг табулирования (дэльта) *)
      p1=-3; p2=3;                   (* Условия существования параметра [0; 1] *)
      rows=5;                        (* Число строк *)
      cells=2;                       (* Число столбцов *)
      chA=11;                        (* Ширина столбца для слова "Аргумент" *)
      chF=10;                        (* Ширина столбца для слова "Функция" *)
      lenA=length('Аргумент') div 2; (* Длина слова "Аргумент" *)
      lenF=length('Функция') div 2;  (* Длина слова "Функция" *)

type T2Val = 1..2;

var x, y, p: single;
    i, j, k: word;
    outputModeForX: T2Val;

procedure writeBorders;
{ *
  * Процедура выводит чёрточки (границы таблицы)
  * }
var k, t: byte;
begin
    write('! ');
    for t:=1 to cells do begin
        for k:=1 to chA do write('-');
        write(' ! ');
        for k:=1 to chF do write('-');
        write(' ! ');
    end;
    writeln;
end;

procedure writeTitle;
{ *
  * Процедура выводит заголовок таблицы табулирования
  * }
var t: byte;
begin
    writeln('---------------------------------------------------');
    writeln('      (c) 2013, Владимир Стадник, 1-я прога');
    writeln('   Программа написана на Debian GNU/Linux 64bit');
    writeln(' И далее протестирована на Linux Mint 15 KDE 64 bit');
    writeln('---------------------------------------------------');
    writeln;
    writeln('Таблица функции с параметром ', p:8);
    writeln;
    writeBorders;
    for t:=1 to cells do begin
      write('! ', 'Агрумент':(chA + lenA), ' ! ', 'Функция':(chF + lenF), ' ');
    end;
    writeln('!');
end;

function tab_f(arg: single): single;
begin
    if arctan(p + arg) >= arg
        then y := (exp(ln(1 + sqr(arg) * sqr(sqr(p)) * 0.2))) / (2 * pi - arg)
        else y := ln(1 + exp(abs(p * arg))) / ln(10);
    tab_f := y;
end;

begin {main}

    clrscr;

    repeat
        write('1. Введите параметр [', p1, ', ', p2, ']: ');
        {$I-} readln(p); {$I+}
    until (p >= p1) and (p <= p2) and (IORESULT = 0);

    (* Доп. задание 5 *)
    outputModeForX := 2;
    writeln('2. Как вы хотите выводить X?');
    writeln('      ''1'' для научного формата;');
    writeln('      ''2'' для экспоненциального формата;');
    write('Введите ваш выбор [', outputModeForX, ']: ');
    readln(outputModeForX);

    x := a;
    j := 0;

    while x <= b + h / 2 do begin
        inc(j);
        clrscr;
        writeTitle;

        for i:=1 to rows do begin                 (* Строки *)
            writeBorders;
            for k:=1 to cells do begin            (* Колонки *)
                y := tab_f(x);                    (* Доп. задание 6 *)
                if outputModeForX = 1 then
                    write('! ', x:chA:(chA - lenA - 1))
                else
                    write('! ', x:chA);
                write(' ! ', y:chF, ' ');
                x := x + h;
            end;
            write('!');
            writeln;
        end;

        writeBorders;
        writeln;
        writeln('Нажмите [Enter] для продолжения...');
        writeln('Экран № ', j);
        readln;
    end;
end.
